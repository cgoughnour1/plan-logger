import logging
import os
import re
from urllib.parse import urlparse

import psycopg2
from psycopg2.extensions import cursor, connection
from sqlalchemy import create_engine
import sqlparse

explainable_re = re.compile(r'^\s*((select)|(delete)|(insert)|(create\s+(temp(orary)?)?\s+table)|(update))', re.I)
whitespace_re = re.compile(r'^\s*$')

log_file_env_var = 'EXPLAIN_PLAN_LOG_FILE'

def create_engine_with_plan_logging(url, break_up_multi_query_strings=False, **kwargs):
    assert urlparse(url).scheme == 'postgresql', 'Only connections via psycopg2 are supported'
    return create_engine('postgresql://', creator=make_plan_logging_creator(url, break_up_multi_query_strings=break_up_multi_query_strings), **kwargs)

def make_plan_logging_creator(url, break_up_multi_query_strings=False):
    if not should_log():
        def vanilla_creator(ignore):
            return psycopg2.connect(url)

        return vanilla_creator

    logger = configure_logger()
    if break_up_multi_query_strings:
        logger.warn('Attempting to break up SQL strings containing multiple queries.')
    class ExplainLoggingCursor(cursor):
        def execute(self, sql, args=None):
            if break_up_multi_query_strings:
                queries = sqlparse.split(self.mogrify(sql, args).decode('utf-8'))
            else:
                queries = [ sql ]
            for query in queries:
                self.log_explain_plan(sqlparse.format(query, strip_comments=True))
                if query:
                    cursor.execute(self, query, args)

        def log_explain_plan(self, query):
            if whitespace_re.match(query):
                return

            if not self.explainable(query):
                logger.info(f"Skipping explain: {query}")
                return

            explain_plan_query = self.make_explain_plan_query(query)
            cursor.execute(self, explain_plan_query)
            explain_plan = self.format_explain_plan(explain_plan_query, cursor.fetchall(self))
            logger.info(explain_plan)

        def make_explain_plan_query(self, query):
            return 'explain {}'.format(query)

        def explainable(self, query):
            return explainable_re.match(query)

        def format_explain_plan(self, explain_plan_query, explain_plan_results):
            return '\n'.join([f'{explain_plan_query};'] + [f'\t{t[0]}' for t in explain_plan_results])

    def plan_logging_creator(ignore):
        return psycopg2.connect(url, cursor_factory=ExplainLoggingCursor)

    return plan_logging_creator

def should_log():
    return os.getenv(log_file_env_var)

def configure_logger():
    logger = logging.getLogger(__name__)
    handler = logging.FileHandler(os.getenv(log_file_env_var))
    handler.setFormatter(logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s'))
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    return logger

if __name__ == '__main__':
    from sqlalchemy import text

    con = create_engine_with_plan_logging(os.getenv('AS_REDSHIFT_URI'), break_up_multi_query_strings=True)

    station_count = con.execute('select count(*) from linear.stations').fetchone()[0]

    print(f'There are {station_count} stations.')

    #con.execute('this syntax is invalid!')

    with con.begin() as txn:
        txn.execute('create temp table tmp_station_names as select name from linear.stations')

        txn.execute('select count(*) from tmp_station_names')

        txn.execute(text('insert into tmp_station_names (name) values (:name)'), name='foo')

        txn.execute("update tmp_station_names set name = 'bar' where name = 'foo'")

        txn.execute(text('delete from tmp_station_names where name = :name'), name='bar')

        txn.execute(text('''
select count(*) from tmp_station_names;
--This is a comment!
select count(*) from tmp_station_names;
       '''))

        txn.execute('drop table tmp_station_names')
