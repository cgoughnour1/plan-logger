Use this library to log query plans for all eligible queries sent to a SQLAlchemy connection.
Currently, only SQLAlchemy connections to Postgresql or Redshift databases are supported.

To use this module, replace this
```
con = sqlalchemy.create_engine(os.getenv('AS_REDSHIFT_URI'))
```
with this
```
con = plan_logger.create_engine_with_plan_logging(os.getenv('AS_REDSHIFT_URI'))
```

As an UNSAFE option, ``create_engine_with_plan_logging`` supports a ``break_up_multi_query_strings`` keyword argument, which causes this module to parse SQL strings into individual queries so that they can be explained and executed in order.

When the environment variable ``EXPLAIN_PLAN_LOG_FILE`` is set, an explain plan will be generated before each query is run and logged to the file whose path is in ``EXPLAIN_PLAN_LOG_FILE``. When the environment variable is not set, explain plans are not generated or logged.
