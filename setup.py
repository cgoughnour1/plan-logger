import setuptools

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name="plan_logger",
    description="Logs explain plans of executed SQL queries",
    version="0.2",
    author="Chris Goughnour",
    author_email="cgoughnour@comscore.com",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=['plan_logger'],
    python_requires='>=3.6',
    zip_safe=True,  
    install_requires=['psycopg2', 'sqlalchemy', 'sqlparse']
)
